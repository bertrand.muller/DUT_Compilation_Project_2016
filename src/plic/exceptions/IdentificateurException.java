package plic.exceptions;

public class IdentificateurException extends Exception {

	public IdentificateurException(String message) {
		super(message);
	}

}
