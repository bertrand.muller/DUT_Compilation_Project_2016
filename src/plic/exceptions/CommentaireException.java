package plic.exceptions;

public class CommentaireException extends Exception {

	public CommentaireException(String message) {
		super(message);
	}

}
