package plic.exceptions;

public class PointVirguleException extends Exception {

	public PointVirguleException(String message) {
		super(message);
	}

}
