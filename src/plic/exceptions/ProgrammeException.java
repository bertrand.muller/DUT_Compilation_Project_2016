package plic.exceptions;

public class ProgrammeException extends Exception {

	public ProgrammeException(String message) {
		super(message);
	}
	
}
