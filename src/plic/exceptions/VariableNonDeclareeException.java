package plic.exceptions;

public class VariableNonDeclareeException extends Exception {

	public VariableNonDeclareeException(String message) {
		super(message);
	}
	
}
