package plic.exceptions;

public class AffectationException extends Exception {

	public AffectationException(String message) {
		super(message);
	}

}
