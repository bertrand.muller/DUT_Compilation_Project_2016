package plic.analyse;

import plic.exceptions.AccoladeException;
import plic.exceptions.AffectationException;
import plic.exceptions.CommentaireException;
import plic.exceptions.IdentificateurException;
import plic.exceptions.PointVirguleException;
import plic.exceptions.ProgrammeException;


/**
 * Analyseur syntaxique pour le langage Plic
 */
public class AnalyseurSyntaxique {

	
	/**
	 * Analyseur lexical du langage Plic
	 */
	private AnalyseurLexical al;
	
	
	/**
	 * Chaine de caracteres courante dans le fichier
	 */
	private String uniteCourante;
	
		
	/**
	 * Constructeur de l'analyseur syntaxique
	 * 
	 * @param nomFich
	 * 		Nom du fichier .plic
	 * 
	 * @throws Exception
	 */
	public AnalyseurSyntaxique(String nomFich) throws Exception {
		
		al = new AnalyseurLexical(nomFich);
		uniteCourante = al.next();
		
	}
	
	
	/**
	 * Méthode permettant d'initialiser le processus 
	 * d'analyse syntaxique du programme contenu dans le fichier .plic
	 * 
	 * @return
	 * 		Bloc du programme
	 * 
	 * @throws Exception
	 */
	public Bloc analyse() throws Exception {
				
		Bloc bl = null;

			
		if(uniteCourante.equals("programme")){
			uniteCourante = al.next();
			this.analyseIdf();
			bl = this.analyseBloc();
		} else {
			throw new ProgrammeException("Le programme Plic ne commence pas par le mot-clé : 'programme'");
		}
		
		return bl;
		
	}

	
	/**
	 * Méthode permettant d'analyser un identificateur
	 * Vérification de la grammaire Plic
	 * 
	 * @return
	 * 		Identificateur analysé
	 * 
	 * @throws Exception 
	 */
	private Idf analyseIdf() throws Exception {
		
		Idf idf = null;
		
		if ((!uniteCourante.matches("^[a-zA-Z]+[a-zA-Z0-9]*"))
		 		|| (uniteCourante.equals("ecrire"))
				|| (uniteCourante.equals("entier"))
				|| (uniteCourante.equals("programme"))) {
				
			throw new IdentificateurException(" L'identificateur n'est pas valide. Symbole inattendu trouvé : '" + uniteCourante + "'");
		}
			
		idf = new Idf(uniteCourante);
		uniteCourante = al.next();
 			
		return idf;
		
	}
	
	
	/**
	 * Méthode permettant d'analyser un bloc
	 * selon la grammaire Plic
	 * 
	 * @return
	 * 		Bloc du programme
	 * 
	 * @throws Exception 
	 */
	private Bloc analyseBloc() throws Exception {

		
		//Création du bloc de programme
		Bloc bl = new Bloc();
		
		if(!uniteCourante.equals("{")) {
			throw new AccoladeException("Une accolade est absente après l'identificateur du programme : '{'");
		}
			
		uniteCourante = al.next();
			
		while(uniteCourante.equals("entier") || uniteCourante.equals("booleen")) {
			this.analyseDeclaration();
		}
				
		//Parcours de l'ensemble du bloc
		while(!uniteCourante.equals("}")) {
								
			switch(uniteCourante) {
				case "{":
					throw new AccoladeException("Une accolade ouvrante supplémentaire a été détectée : '{'");					
					
				case "eof":
					throw new AccoladeException("Une accolade est absente pour fermer un bloc du programme : '}'");
					
				case "*/":
					throw new CommentaireException("Fin de commentaire inattendu : '*/'");
						
				case "/*":
					throw new CommentaireException("Début de dommentaire inattendu : '/*'");	
							
				default:
					Instruction i = analyseInstruction();
					bl.ajouterInstruction(i);
			}
				
		}
			
		if(!uniteCourante.equals("}")) {
			throw new AccoladeException("Une accolade '}' est manquante en fin de bloc.");
		}
			
		uniteCourante = al.next();
		return bl;
		
	}
	
	
	/**
	 * Fonction permettant d'entamer le processus 
	 * d'analyse d'une instruction :
	 * 		- Affichage
	 * 		- Déclaration
	 * 		- Affectation
	 * 
	 * @param uniteCourante
	 * 		Chaîne de caractères courante
	 * 
	 * @return
	 * 		Instruction présente dans un bloc
	 * 
	 * @throws Exception
	 */
	private Instruction analyseInstruction() throws Exception {
		
		Instruction i = null;
		
		switch(uniteCourante) {		
			case "ecrire":
				i = this.analyseAffichage();
				break;
				
			default:
				i = this.analyseAffectation();
				break;
		}
		
		return i;
	}
	
	
	/**
	 * Méthode d'analyse d'une déclaration
	 * selon la grammaire Plic
	 * 
	 * @throws Exception
	 */
	private void analyseDeclaration() throws Exception {
		
		String type;
		
		if(uniteCourante.equals("entier")) {
			type = "entier";
		} else {
			type = "booleen";
		}
		
		uniteCourante = al.next();
		
		while (!uniteCourante.equals(";")) {
			Idf idf = this.analyseIdf();
			Tds.getInstance().ajouter(idf, type);
		}
		
		uniteCourante = al.next();
		
	}
	
	
	/**
	 * Méthode permettant d'analyser une affectation
	 * selon la grammaire Plic
	 * 
	 * @return
	 * 		Instruction d'affectation analysée
	 * 
	 * @throws Exception
	 */
	private Affectation analyseAffectation() throws Exception {
		
		Affectation aff = null;
		Idf idf = this.analyseIdf();
			
		if(!uniteCourante.equals(":=")) {
			throw new AffectationException("Le symbole ':=' n'a pas été trouvé lors de l'affectation.");
		}
		
		uniteCourante = al.next(); 
		
		if(uniteCourante.equals("vrai") || uniteCourante.equals("faux")) {
			Booleen b = analyseBooleen();
			aff = new Affectation(idf, b);
		} else {
			Nombre n = analyseNombre();
			aff = new Affectation(idf, n);
		}
		
		uniteCourante = al.next();
		
		if(!uniteCourante.equals(";")) {
			throw new PointVirguleException("Un point-virgule ';' est manquant ou mal placé à la fin de l'instruction d'affectation");
		}
			
		uniteCourante = al.next();
		
		return aff;
		
	}
	
	
	/**
	 * Méthode permettant d'analyser un booleen selon
	 * la grammaire Plic
	 * 
	 * @return
	 * 		Booleen analysé
	 * 
	 * @throws Exception
	 */
	public Booleen analyseBooleen() throws Exception {
		
		boolean bool;
			
		switch(uniteCourante) {
			
			case "vrai":
				bool = true;
				break;
			
			case "faux":
				bool = false;
				break;
			
			default:
				throw new AffectationException("Lors de l'affectation une valeur inattendue a été trouvée : '" + uniteCourante + "'"); 
	
		}
		
		Booleen b = new Booleen(bool);
		return b; 
		
	}
	
	
	/**
	 * Méthode permettant d'analyser un nombre selon
	 * la grammaire Plic
	 * 
	 * @return
	 * 		Nombre analysé
	 * 
	 * @throws Exception
	 */
	public Nombre analyseNombre() throws Exception {
		
		Nombre n = null;
		
		try {
			
			int nombre = Integer.parseInt(uniteCourante);
			n = new Nombre(nombre);
			
		} catch (NumberFormatException e) {
			throw new AffectationException("Lors de l'affectation une valeur inattendue a été trouvée : '" + uniteCourante + "'");
		} 	
		
		return n;
		
	}
	
	
	/**
	 * Fonction permettant d'analyser l'affichage
	 * selon la grammaire Plic
	 * 
	 * @return
	 * 		Instruction d'affichage analysée
	 * 
	 * @throws Exception
	 */
	private Ecrire analyseAffichage() throws Exception {

		Ecrire affich = new Ecrire();
		uniteCourante = al.next();
		
		while (!uniteCourante.equals(";")) {
			Idf idf = this.analyseIdf();
			affich.ajouterIdf(idf);
		}
		
		uniteCourante = al.next();
		
		return affich;
		
	}

}
