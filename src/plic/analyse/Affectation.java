package plic.analyse;


/**
 * Classe modélisant une instruction d'affectation
 */
public class Affectation implements Instruction {

	
	/**
	 * Identificateur d'une variable
	 */
	private Idf idf;
	
	
	/**
	 * Valeur associée à la variable avec identificateur idf
	 */
	private Type type;
	
	
	/**
	 * Constructeur d'une affectation
	 * 
	 * @param idf
	 * 		Identificateur d'une variable
	 * 
	 * @param n
	 * 		Valeur entière affectée à la variable
	 */
	public Affectation(Idf idf, Type t) {
		this.idf = idf;
		this.type = t;
	}
	
	
	/**
	 * Méthode permettant de retourner la chaîne
	 * de caractères de l'identificateur d'une variable
	 * 
	 * @return
	 * 		Identificateur de la variable
	 */
	public Idf getIdf() {
		return this.idf;
	}
	
	
	/**
	 * Méthode permettant de retourner la valeur
	 * associée à la variable
	 * 
	 * @return
	 * 		Valeur de la variable
	 */
	public Type getType() {
		return this.type;
	}


	@Override
	/**
	 * Méthode permettant de vérifier une instruction
	 * d'affectation sémantiquement
	 * 
	 * @throws Exception
	 */
	public void verifier() throws Exception {
		this.idf.verifier();
	}


	@Override
	/**
	 * Méthode générant le code MIPS liée à une
	 * affectation et le renvoit sous forme de chaînes de caractères
	 * 
	 * @return
	 * 		Code MIPS de l'affectation
	 */
	public String toMips() {
		
		String mips = "";
		int deplacement = this.idf.getSymbole().getDeplacement();
		String nomIdf = this.idf.getIdf();
		
		if(this.type instanceof Nombre) {
			
			Nombre nombre = (Nombre) this.type;
			int nb = nombre.getNb();
		
			mips += "\t# " + nomIdf + " = " + nb + "\n"
				 + "\tli $a0," + nb + "\n"
				 + "\tsw $a0," + deplacement + "($s7)\n\n";
			
		} else {
			
			Booleen bool = (Booleen) this.type;
			boolean b = bool.getBool();
			String s = (b) ? "vrai" : "faux";
			int valMipsBool = (b) ? 1 : 0;
			
			mips += "\t# " + nomIdf + " = " + s + " (" + valMipsBool +")\n"
				  + "\tli $a0," + valMipsBool +"\n"
				  + "\tsw $a0," + deplacement + "($s7)\n\n";
			
		}
		
		return mips;
		
	}
	
}
