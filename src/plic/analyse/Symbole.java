package plic.analyse;


/**
 * Classe modélisant un symbole
 * associé à un identificateur
 */
public class Symbole {

	
	/**
	 * Type de l'identificateur
	 */
	private String type;
	
	
	/**
	 * Valeur de déplacement de l'identificateur
	 * dans le code MIPS
	 */
	private int deplac;
	
	
	/**
	 * Constructeur d'un symbole
	 * 
	 * @param t
	 * 		Type de l'identificateur
	 * 
	 * @param tailleBloc
	 * 		Taille actuelle du bloc où se trouve le symbole
	 */
	public Symbole(String t, int tailleBloc) {
		this.type = t;
		this.deplac = tailleBloc;
	}
	
	
	/**
	 * Méthode renvoyant le type 
	 * de l'identificateur
	 * 
	 * @return
	 * 		Type de l'identificateur
	 */
	public String getType() {
		return this.type;
	}
	
	
	/**
	 * Méthode renvoyant la valeur du 
	 * déplacement de l'identificateur
	 * 
	 * @return
	 * 		Valeur du déplacement
	 */
	public int getDeplacement() {
		return this.deplac;
	}
	
}
