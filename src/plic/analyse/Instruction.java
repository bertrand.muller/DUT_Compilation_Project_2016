package plic.analyse;


/**
 * Interface modélisant une instruction
 */
public interface Instruction {

	
	/**
	 * Méthode abstraite permettant de vérifier
	 * l'instruction sémantiquement
	 * 
	 * @throws Exception 
	 */
	public void verifier() throws Exception;
	
	
	/**
	 * Méthode permettant de générer le code Mips
	 * associé à une instruction Plic
	 */
	public String toMips();
	
}
