package plic.analyse;


/**
 * Classe modélisant un booléen
 */
public class Booleen implements Type {

	
	/**
	 * Valeur associée au booléen
	 */
	private boolean bool;
	
	
	/**
	 * Constructeur d'un booléen
	 * 
	 * @param b
	 * 		Valeur booléenne associée
	 */
	public Booleen (boolean b) {
		this.bool = b;		
	}
	
	
	/**
	 * Méthode retournant la valeur booléenne
	 * 
	 * @return
	 * 		Valeur booléenne
	 */
	public boolean getBool() {
		return this.bool;
	}
	
	
}
