package plic.analyse;


/**
 * Classe modélisant un identificateur
 */
public class Idf {

	
	/**
	 * Chaîne de caractères pour un identificateur
	 */
	private String idf;
	
	
	/**
	 * Symbole associé à l'identificateur
	 */
	private Symbole symbole;
	
	
	/**
	 * Constructeur d'un identificateur
	 * 
	 * @param idf
	 * 		Chaîne de caractères associée à l'identificateur
	 */
	public Idf(String idf) {
		this.idf = idf;
	}
	
	
	/**
	 * Méthode renvoyant l'identificateur
	 * 
	 * @return
	 * 		Chaîne de caractères de l'identificateur
	 */
	public String getIdf() {
		return this.idf;
	}
	
	
	@Override
	/**
	 * Méthode permettant de générer un entier unique
	 * pour chaque identificateur (utile pour la HashMap)
	 */
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((idf == null) ? 0 : idf.hashCode());
		return result;
	}


	@Override
	/**
	 * Méthode permettant de savoir si deux idfs
	 * sont identiques au niveau de leur nom (utile pour la HashMap)
	 */
	public boolean equals(Object obj) {
		
		Idf idf2 = (Idf) obj;
		String nom2 = idf2.getIdf();
		return (this.idf.equals(nom2));
		
	}
	
	
	/**
	 * Méthode permettant d'initialiser le processus
	 * de vérification d'un identificateur
	 * 
	 * @throws Exception
	 */
	public void verifier() throws Exception {
		this.symbole = Tds.getInstance().identifier(this);
	}
	
	
	public Symbole getSymbole() {
		return this.symbole;
	}
	
}
