package plic.analyse;

import java.util.HashMap;

import plic.exceptions.DoubleDeclarationException;
import plic.exceptions.VariableNonDeclareeException;


/**
 * Classe modélisant une table des symboles
 */
public class Tds {

	
	/**
	 * Table d'identificateurs associés à leur type
	 */
	private HashMap<Idf,Symbole> tabSymboles;
	
	
	/**
	 * Instance d'une table de symboles
	 */
	private static Tds tds;
	
	
	/**
	 * Taille du bloc en octets
	 */
	private int tailleBloc;
	
	
	/**
	 * Constructeur d'une table de symboles
	 */
	private Tds(){
		this.tabSymboles = new HashMap<Idf, Symbole>();
		this.tailleBloc = 0;
	}
	
	
	/**
	 * Méthode permettant d'obtenir une instance
	 * d'une table de symboles
	 * 
	 * @return
	 * 		Table de symboles
	 */
	public static Tds getInstance() {
		
		if(tds == null) {
			tds = new Tds();
		}
		
		return tds;
		
	}
	
	
	/**
	 * Méthode permettant d'ajouter un nouvel identificateur
	 * dans la table, en vérifiant qu'il n'a pas été ajouté précédemment
	 * 
	 * @param nom
	 * 		Nom de l'identificateur
	 * 
	 * @param type
	 * 		Type de l'identifcateur (entier ou booléen)
	 * 
	 * @throws Exception
	 */
	public void ajouter(Idf idf, String type) throws Exception {
		
		if(tabSymboles.containsKey(idf)) {
			throw new DoubleDeclarationException("L'identificateur '" + idf.getIdf() + "' a déjà été déclaré.");
		} else {
			Symbole symb = new Symbole(type,this.tailleBloc);
			this.tailleBloc -= 4;
			tabSymboles.put(idf, symb);
		}
	
	}	
	
	
	/**
	 * Méthode permettant de vérifier que l'identificateur
	 * existe bien dans la table
	 * 
	 * @throws Exception
	 */
	public Symbole identifier(Idf idf) throws Exception {
	
		Symbole symb = null;
		
		if(this.tabSymboles.containsKey(idf)) {
			symb = this.tabSymboles.get(idf);
		} else {
			throw new VariableNonDeclareeException("La variable '" + idf.getIdf() + "' n'a pas été déclarée.");
		}
		
		return symb;
		
	}
	
	
	/**
	 * Méthode permettant d'obtenir
	 * la taille du bloc en octets
	 *  
	 * @return
	 * 		Taille du bloc
	 */
	public int getTailleBloc() {
		return this.tailleBloc;
	}
	
	
	public Symbole getSymbole(String nomIdf) {
		
		for(Idf idf : this.tabSymboles.keySet()) {
			
			if(idf.getIdf().equals(nomIdf)) {
				return this.tabSymboles.get(idf);
			}
			
		}
		
		return null;
		
	}
	
	
}
