package plic.analyse;

import java.util.ArrayList;


/**
 * Classe modélisant un bloc de programme
 */
public class Bloc {

	
	/**
	 * Liste d'instructions contenues dans le bloc
	 */
	private ArrayList<Instruction> instructions;
	
	
	/**
	 * Taille en octets du bloc
	 */
	private int taille;
	
	
	/**
	 * Constructeur d'un bloc
	 */
	public Bloc() {
		this.instructions = new ArrayList<Instruction>();
		this.taille = 0;
	}
	
	
	/**
	 * Méthode permettant d'ajouter une instruction
	 * à la liste
	 * 
	 * @param i
	 * 		Instruction à ajouter
	 */
	public void ajouterInstruction(Instruction i) {
		this.instructions.add(i);
	}


	/**
	 * Méthode permettant de vérifier les instructions
	 * une par une et sémantiquement 
	 * 
	 * @throws Exception
	 */
	public void verifier() throws Exception {
		
		for(Instruction i : this.instructions) {
			i.verifier();
		}
		
		this.taille = Tds.getInstance().getTailleBloc();
		
	}
	
	
	/**
	 * Méthode permettant de générer le code MIPS
	 * associé au bloc d'instructions écrits en langage Plic
	 * 
	 * @return
	 * 		Code MIPS correspondant au bloc
	 */
	public String toMips() {
		
		String mips = ".data\n" 
					+ "lus: .space 256\n"
					+ "retourALaLigne: .asciiz \"\\n\" \n"
					+ "faireEspace: .asciiz \" \" \n"
					+ "afficherVrai: .asciiz \"vrai\"\n"
					+ "afficherFaux: .asciiz \"faux\"\n"
					+ ".text \n\n"
					
					+ "main :\n\n"
					
					+ "\t# Valeur pour tester les booleens\n"					
					+ "\tli $t0,0\n\n"
					
					+ "\t# Initialisation de s7 avec sp (initialisation de la base des variables)\n"
					+ "\tmove $s7,$sp\n\n"
					+ "\t# Allocation de la mémoire (" + this.taille + ")\n"
					+ "\taddi $sp,$sp," + this.taille + "\n\n";
		
		for (Instruction i : this.instructions) {
			mips += i.toMips();
		}
		
		mips += "end : \n\n"
			  + "\t# Code de fin\n"
			  + "\tli $v0,10\n"
			  + "\tsyscall";
		
		return mips;
		
	}
	
}
