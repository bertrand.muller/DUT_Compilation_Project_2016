package plic.analyse;
import java.io.File;
import java.util.NoSuchElementException;
import java.util.Scanner;


/**
 * Analyseur lexical pour le langage Plic
 */
public class AnalyseurLexical {

	
	/**
	 * Lecteur d'un fichier d'extension .plic
	 */
	private Scanner lecteur;
	
	
	/**
	 * Constructeur d'un analyseur lexical
	 * 
	 * @param nomF
	 * 		Nom du fichier .plic
	 * 
	 * @throws Exception
	 */
	public AnalyseurLexical(String nomF) throws Exception {
		
		this.lecteur = new Scanner(new File(nomF));
		
	}
	
	
	/**
	 * Méthode renvoyant à chaque appel la
	 * prochaine chaîne de caractères du fichier .plic
	 * 
	 * @return
	 * 		Chaîne de caractères présente dans le fichier .plic
	 * 
	 * @throws Exception
	 */
	public String next() {
		
		try {
		
			String proChaine = this.lecteur.next();
			
			if(proChaine.equals("/*")) {
				while(proChaine.equals("/*")) {
					while(!proChaine.equals("*/")) {
						proChaine = this.lecteur.next();
					}
					
					proChaine = this.lecteur.next();
				}	
			}
			
			return proChaine;
			
		} catch(NoSuchElementException eof) {
			return "eof";
		}
	}
}
