package plic.analyse;


/**
 * Classe modélisant un nombre entier
 */
public class Nombre implements Type {

	
	/**
	 * Nombre entier présent dans un programme Plic
	 */
	private int nb;
	
	
	/**
	 * Constructeur d'un nombre
	 * 
	 * @param nomb
	 * 		Valeur entière associée à ce nombre
	 */
	public Nombre(int nomb) {
		this.nb = nomb;
	}
	
	
	/**
	 * Méthode retournant le nombre entier
	 * 
	 * @return
	 * 		Valeur entière du nombre
	 */
	public int getNb() {
		return this.nb;
	}
	
}
