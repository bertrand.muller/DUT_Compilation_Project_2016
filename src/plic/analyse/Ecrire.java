package plic.analyse;

import java.util.ArrayList;


/**
 * Classe modélisant une instruction d'affichage
 */
public class Ecrire implements Instruction {

	
	/**
	 * Liste d'identificateurs de variables à afficher
	 */
	private ArrayList<Idf> idfs;
	
	
	/**
	 * Nombre de booleens affichés
	 */
	private static int nbBooleens = 0;
	
	
	/**
	 * Constructeur d'une instruction d'affichage
	 */
	public Ecrire() {
		this.idfs = new ArrayList<Idf>();
	}
	
	
	/**
	 * Méthode permettant d'ajouter un identificateur
	 * dans la liste
	 * 
	 * @param idf
	 * 		Identificateur à ajouter
	 */
	public void ajouterIdf(Idf idf) {
		this.idfs.add(idf);
	}


	@Override
	/**
	 * Méthode permettant de vérifier une instruction
	 * d'affichage sémantiquement
	 * 
	 * @throws Exception
	 */
	public void verifier() throws Exception {
		
		for(Idf idf : this.idfs) {
			idf.verifier();
		}
		
	}


	@Override
	/**
	 * Méthode générant le code MIPS liée à une série
	 * d'affichages et le renvoit sous forme de chaînes de caractères
	 * 
	 * @return
	 * 		Code MIPS de l'affectation
	 */
	public String toMips() {
		
		String mips = "";
		
		for(Idf idf : this.idfs) {
			
			int deplacement = idf.getSymbole().getDeplacement();
			String type = idf.getSymbole().getType();
			String chaineIdf = idf.getIdf();
			
			mips += "\t# Affichage de la variable " + chaineIdf + "\n";
			
			if(type.equals("entier")) {
				
				mips += "\tli $v0,1\n"
				      + "\tlw $a0," + deplacement + "($s7)\n"
					  + "\tsyscall\n\n";
				  
			} else {
				
				Ecrire.nbBooleens++;
				int numBooleen = Ecrire.nbBooleens;
				
				mips += "\tlw $a0," + deplacement + "($s7)\n"
					  + "\tbeq $a0,$t0,AFFFAUX" + numBooleen + "\n"
					  + "\tli $v0,4\n"
					  + "\tla $a0,afficherVrai\n"
					  + "\tsyscall\n"
					  + "\tj FINAFF" + numBooleen + "\n\n"
				
					  + "\t# Affichage de la chaine 'faux'\n"
					  + "\tAFFFAUX" + numBooleen + ":\n"
					  + "\t\tli $v0,4\n"
					  + "\t\tla $a0,afficherFaux\n"
					  + "\t\tsyscall\n\n"
					  + "\t# Saut de fin d'affichage\n"
					  + "\tFINAFF" + numBooleen + ":\n\n";
				
			}
			
			mips += "\t# Retour à la ligne\n"
				  + "\tli $v0,4\n"
				  + "\tla $a0,retourALaLigne\n"
				  + "\tsyscall\n\n";
			
		}
		
		return mips;
	}
	
}
