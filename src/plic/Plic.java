package plic;

import java.io.File;
import java.io.FileWriter;

import plic.analyse.AnalyseurSyntaxique;
import plic.analyse.Bloc;


/**
 * Classe principale
 */
public class Plic {

	
	/**
	 * Analyseur syntaxique associé à l'analyse du fichier
	 */
	private AnalyseurSyntaxique as;
	
	
	/**
	 * @param nomF
	 * 		Nom du fichier à analyser
	 * 
	 * @throws Exception
	 */
	public Plic(String nomF) throws Exception {
		as = new AnalyseurSyntaxique(nomF);
	}

	
	/**
	 * Méthode principale lancant l'analyse
	 * 
	 * @param args
	 * 		Nom du fichier passé en argument
	 */
	public static void main(String[] args) {
		
		try {
			
			String nomFichierSource = args[0];
			Plic pl = new Plic(nomFichierSource);
			Bloc b = pl.as.analyse();
			b.verifier();
			
			String nomFichierMips = nomFichierSource.split(".plic")[0].concat(".mips");
			FileWriter fw = new FileWriter(new File(nomFichierMips));
			fw.write(b.toMips());
			fw.close();
			
		} catch(Exception e) {
			System.out.println(e.getMessage());
		}
	}

}
