.data
lus: .space 256
retourALaLigne: .asciiz "\n" 
faireEspace: .asciiz " " 
afficherVrai: .asciiz "vrai"
afficherFaux: .asciiz "faux"
.text 

main :

	# Valeur pour tester les booleens
	li $t0,0

	# Initialisation de s7 avec sp (initialisation de la base des variables)
	move $s7,$sp

	# Allocation de la m�moire (-24)
	addi $sp,$sp,-24

	# i = 31
	li $a0,31
	sw $a0,0($s7)

	# j = 5
	li $a0,5
	sw $a0,-4($s7)

	# k = 6
	li $a0,6
	sw $a0,-8($s7)

	# b1 = vrai (1)
	li $a0,1
	sw $a0,-16($s7)

	# b2 = faux (0)
	li $a0,0
	sw $a0,-20($s7)

	# Affichage de la variable b1
	lw $a0,-16($s7)
	beq $a0,$t0,AFFFAUX1
	li $v0,4
	la $a0,afficherVrai
	syscall
	j FINAFF1

	# Affichage de la chaine 'faux'
	AFFFAUX1:
		li $v0,4
		la $a0,afficherFaux
		syscall

	# Saut de fin d'affichage
	FINAFF1:

	# Retour � la ligne
	li $v0,4
	la $a0,retourALaLigne
	syscall

	# Affichage de la variable b2
	lw $a0,-20($s7)
	beq $a0,$t0,AFFFAUX2
	li $v0,4
	la $a0,afficherVrai
	syscall
	j FINAFF2

	# Affichage de la chaine 'faux'
	AFFFAUX2:
		li $v0,4
		la $a0,afficherFaux
		syscall

	# Saut de fin d'affichage
	FINAFF2:

	# Retour � la ligne
	li $v0,4
	la $a0,retourALaLigne
	syscall

	# Affichage de la variable i
	li $v0,1
	lw $a0,0($s7)
	syscall

	# Retour � la ligne
	li $v0,4
	la $a0,retourALaLigne
	syscall

	# Affichage de la variable j
	li $v0,1
	lw $a0,-4($s7)
	syscall

	# Retour � la ligne
	li $v0,4
	la $a0,retourALaLigne
	syscall

	# Affichage de la variable k
	li $v0,1
	lw $a0,-8($s7)
	syscall

	# Retour � la ligne
	li $v0,4
	la $a0,retourALaLigne
	syscall

	# k = 1243
	li $a0,1243
	sw $a0,-8($s7)

	# Affichage de la variable k
	li $v0,1
	lw $a0,-8($s7)
	syscall

	# Retour � la ligne
	li $v0,4
	la $a0,retourALaLigne
	syscall

	# Affichage de la variable b1
	lw $a0,-16($s7)
	beq $a0,$t0,AFFFAUX3
	li $v0,4
	la $a0,afficherVrai
	syscall
	j FINAFF3

	# Affichage de la chaine 'faux'
	AFFFAUX3:
		li $v0,4
		la $a0,afficherFaux
		syscall

	# Saut de fin d'affichage
	FINAFF3:

	# Retour � la ligne
	li $v0,4
	la $a0,retourALaLigne
	syscall

end : 

	# Code de fin
	li $v0,10
	syscall