main :

	# initialiser s7 avec sp (initialisation de la base des variables)
	move $s7,$sp
	
	# réservation de l'espace pour 1 variable
	addi $sp, $sp, -4
	
	# a = 18
	li $v0,18
	sw $v0,0($s7)
	
	# lire a
	li $v0,1
	lw $a0,0($s7)
	syscall
	
end : # fin de programme

	li $v0,10
	syscall	